#include <OneWire.h>
OneWire ds(7);

//Find setup function at line 228 and loop at 237

//definitions for pHmeter
#define SensorPin A4            //pH meter Analog output to Arduino Analog Input 4
#define Offset 0.00            //deviation compensate
#define samplingInterval 20
#define printInterval 250
#define ArrayLenth  10    //times of collection

//definitions for temperature
#define DS18S20_ID 0x10
#define DS18B20_ID 0x28

//global variables for pH
int pHArray[ArrayLenth];   //Store the average value of the sensor feedback
int pHArrayIndex = 0;
static float pHValue;

//global variables for Temperature
float temp;

//other global variables
static unsigned long time_s, time_c;
int TSS, TDS, OG, TC, S, PC, BOD;
char MAC[17];
volatile double waterFlow;
float flow_r;


//stock values for all sensors
static float pHValue_s = 7.0;
float temp_s = 25.5;
volatile double waterFlow_s = 100;
char MAC_s[] = "00000000c8e6cd58"; //eth0 MAC b8:27:eb:e6:cd:58 wlan0 MAC b8:27:eb:b3:98:0d serial 00000000c8e6cd58
int TSS_s = 1000;
int TDS_s = 2100;
int OG_s = 10;
int TC_s = 2;
int S_s = 2;     //sulphide
int PC_s = 5;
int BOD_s = 150;
int standby_mode = 0;     //setting 1 will print stock values instead sensor readings


void MAC_read() {
  //MAC = MAC_s;
}

void MAC_print() {
  Serial.print("MAC");
  Serial.print(",");
  Serial.print(MAC_s);
  Serial.print(",");
}

void pH_read() {
  static unsigned long samplingTime = millis();
  static float voltage_ph;
  if (millis() - samplingTime > samplingInterval)
  {
    pHArray[pHArrayIndex++] = analogRead(SensorPin);
    if (pHArrayIndex == ArrayLenth)pHArrayIndex = 0;
    voltage_ph = avergearray(pHArray, ArrayLenth) * 5.0 / 1024;
    pHValue = 3.5 * voltage_ph + Offset;
    samplingTime = millis();
  }
}

void pH_print() {
  Serial.print("PH");
  Serial.print(",");
  Serial.print(pHValue, 2);
  Serial.print(",");
}

void TSS_read() {
  int sensorValue = analogRead(A1);// read the input on analog pin A1:
  float voltage_tss = sensorValue * (5.0 / 1024.0); // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  TSS = (-1120.4 * (voltage_tss * voltage_tss)) + (5742.3 * voltage_tss) - 4352.9;
}

void TSS_print() {
  Serial.print("TSS");
  Serial.print(",");
  Serial.print(TSS / 10);
  Serial.print(",");
}

//temperature measurement
boolean getTemperature() {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  //find a device
  if (!ds.search(addr)) {
    ds.reset_search();
    //Serial.print("Problem at 1,");
    return false;
  }
  if (OneWire::crc8( addr, 7) != addr[7]) {
    //Serial.print("Problem at 2,");
    return false;
  }
  if (addr[0] != DS18S20_ID && addr[0] != DS18B20_ID) {
    //Serial.print("Problem at 3,");
    return false;
  }
  ds.reset();
  ds.select(addr);
  // Start conversion
  ds.write(0x44, 1);
  // Wait some time...
  delay(850);
  present = ds.reset();
  ds.select(addr);
  // Issue Read scratchpad command
  ds.write(0xBE);
  // Receive 9 bytes
  for ( i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
  // Calculate temperature value
  temp = ( (data[1] << 8) + data[0] ) * 0.0625;
  return true;
}

void Temp_read() {
  getTemperature();
}

void Temp_print() {
  Serial.print("T");
  Serial.print(",");
  if (getTemperature()) {
    temp_s = temp;
    Serial.print(temp);
    Serial.print(",");
  }
  else {
    Serial.print (temp_s);
    Serial.print(",");
  }
}

void WWF_read() {
  time_c = (millis() / 1000);
  flow_r = (waterFlow / time_c) * 60;

}

void WWF_print() {
  Serial.print("WWF");
  Serial.print(",");
  Serial.print(flow_r);
  Serial.print(",");
}

void TDS_read() {
  TDS = TDS_s;
}

void TDS_print() {
  Serial.print("TDS");
  Serial.print(",");
  Serial.print(TDS);
  Serial.print(",");
}

void OG_read() {
  OG = OG_s;
}

void OG_print() {
  Serial.print("OG");
  Serial.print(",");
  Serial.print(OG);
  Serial.print(",");
}

void TC_read() {
  TC = TC_s;
}

void TC_print() {
  Serial.print("TC");
  Serial.print(",");
  Serial.print(TC);
  Serial.print(",");
}

void S_read() {
  S = S_s;
}

void S_print() {
  Serial.print("S");
  Serial.print(",");
  Serial.print(S);
  Serial.print(",");
}

void PC_read() {
  PC = PC_s;
}

void PC_print() {
  Serial.print("PC");
  Serial.print(",");
  Serial.print(PC);
  Serial.print(",");
}

void BOD_read() {
  BOD = BOD_s;
}

void BOD_print() {
  Serial.print("BOD");
  Serial.print(",");
  Serial.print(BOD);
  Serial.print(",");
}

void setup() {
  Serial.begin(9600);
  waterFlow = 0;
  attachInterrupt(0, pulse, RISING);  //DIGITAL Pin 2: Interrupt 0
  time_s = millis() / 1000;
}

uint8_t cmd = 0;

void loop() {
  if (Serial.available())
  {
    cmd = Serial.read();
    if (cmd == 99)
    {
      if (standby_mode) standby_m();
      else {
        MAC_read();
        pH_read();
        TSS_read();
        //Temp_read();
        WWF_read();
        TDS_read();
        OG_read();
        TC_read();
        S_read();
        PC_read();
        BOD_read();

        //Serial.println("Printing from source 1");
        Serial.print("S");
        Serial.print(",");
        Serial.print("23");
        Serial.print(",");
        MAC_print();
        pH_print();
        TSS_print();
        Temp_print();
        WWF_print();
        TDS_print();
        OG_print();
        TC_print();
        S_print();
        PC_print();
        BOD_print();
        Serial.println("E");
      }
    }
  }

}


void pulse()   //measure the quantity of square wave
{
  waterFlow += 1.0 / 450.0;
}

double avergearray(int* arr, int number) {
  int i;
  int max, min;
  double avg;
  long amount = 0;
  if (number <= 0) {
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if (number < 5) { //less than 5, calculated directly statistics
    for (i = 0; i < number; i++) {
      amount += arr[i];
    }
    avg = amount / number;
    return avg;
  } else {
    if (arr[0] < arr[1]) {
      min = arr[0]; max = arr[1];
    }
    else {
      min = arr[1]; max = arr[0];
    }
    for (i = 2; i < number; i++) {
      if (arr[i] < min) {
        amount += min;      //arr<min
        min = arr[i];
      } else {
        if (arr[i] > max) {
          amount += max;  //arr>max
          max = arr[i];
        } else {
          amount += arr[i]; //min<=arr<=max
        }
      }//if
    }//for
    avg = (double)amount / (number - 2);
  }//if
  return avg;
}

void standby_m() {
  //Serial.println("Printing from source 2");
  Serial.print("S");         //start
  Serial.print(",");
  Serial.print("23");
  Serial.print(",");
  Serial.print("MAC");
  Serial.print(",");
  Serial.print(MAC_s);
  Serial.print(",");
  Serial.print("PH");
  Serial.print(",");
  Serial.print(pHValue_s);
  Serial.print(",");
  Serial.print("TSS");
  Serial.print(",");
  Serial.print(TSS_s / 10);
  Serial.print(",");
  Serial.print("T");
  Serial.print(",");
  Serial.print(temp_s);
  Serial.print(",");
  Serial.print("WWF");
  Serial.print(",");
  Serial.print(waterFlow_s);
  Serial.print(",");
  Serial.print("TDS");
  Serial.print(",");
  Serial.print(TDS_s);
  Serial.print(",");
  Serial.print("OG");
  Serial.print(",");
  Serial.print(OG_s);
  Serial.print(",");
  Serial.print("TC");
  Serial.print(",");
  Serial.print(TC_s);
  Serial.print(",");
  Serial.print("S");
  Serial.print(",");
  Serial.print(S_s);
  Serial.print(",");
  Serial.print("PC");
  Serial.print(",");
  Serial.print(PC_s);
  Serial.print(",");
  Serial.print("BOD");
  Serial.print(",");
  Serial.print(BOD_s);
  Serial.print(",");
  Serial.println("E");     //end
  //delay(500);
}
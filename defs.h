#include <OneWire.h>
#include <DallasTemperature.h>

//Now we define the pins for the temperature sensors
#define NO_TEMP_SENSOR 4
OneWire o1(7);
OneWire o2(8);
OneWire o3(9);
OneWire o4(10);
DallasTemperature s1(&o1);
DallasTemperature s2(&o2);
DallasTemperature s3(&o3);
DallasTemperature s4(&o4);


uint8_t tempaddr[8] = {0x28, 0xFF, 0xEB, 0xC6, 0x83, 0x16, 0x03, 0xF0};
float temp_out[NO_TEMP_SENSOR];

//Now we define the PT resistor array
//This would contain the resistor values

#define NO_PT_SENSOR 4
uint8_t PT_sensors[NO_PT_SENSOR]={175,177,177,177};
#define MP_F 4.82
#define NO_SAMPLE 30
#define first_analog A0
double current_array[NO_PT_SENSOR];
//Now we define the relay pins
#define RL1 A4
#define RL2 A5
#define CONFIG_RL_IO pinMode(RL1,OUTPUT);pinMode(RL2,OUTPUT)
#define RL1_ON digitalWrite(RL1,HIGH)
#define RL1_OFF digitalWrite(RL1,LOW)
#define RL2_ON digitalWrite(RL2,HIGH)
#define RL2_OFF digitalWrite(RL2,LOW)


//data buffer definitons
#define START_BYTE 0x53
#define END_BYTE 0x45
#define START_TEMP 0x54
#define END_TEMP 0x55
#define START_PRESSURE 0x50
#define END_PRESSURE 0x51


//misc variables
int loop_var=0;
#define LED_PIN 6
#define LED_CONFIG pinMode(LED_PIN,OUTPUT)
#define LED_ON digitalWrite(LED_PIN,HIGH)
#define LED_OFF digitalWrite(LED_PIN,LOW)
int delay_cnt;
byte spp_cmd;





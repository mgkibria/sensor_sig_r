#include "defs.h"

int TSS_HIGH = 100;                           //set TSS mapping range here
int WWF_HIGH = 100;                           //set WWF mapping range here
int TDS_HIGH = 100;                           //set TDS mapping range here

int TSS, TDS, WWF;
float Temp, PH;

void get_temps(float temp_array[])
{
  uint8_t i = 0;
  s1.requestTemperatures();
  temp_array[0] = s1.getTempCByIndex(0);
}

void setup() {
  Serial.begin(9600);
  s1.begin();


  delay(1000);
}

void read_sensor() {
  int PH_r = analogRead(A0);                  //reads ph analog value
  PH = map(PH_r, 0, 1023, 0, 14);       //maps to ph scale

  int TSS_r = analogRead(A1);                  //reads TSS analog value
  TSS = map(TSS_r, 0, 1023, 0, TSS_HIGH);  //maps to TSS scale

  get_temps(temp_out);                         //Reads temp
  Temp = temp_out[0];

  int WWF_r = analogRead(A2);                 //reads WWF analog value
  WWF = map(WWF_r, 0, 1023, 0, WWF_HIGH);   //maps to WWF scale

  int TDS_r = analogRead(A3);                  //reads TDS analog value
  TDS = map(TDS_r, 0, 1023, 0, TDS_HIGH);  //maps to TDS scale

}

void send_data() {

  Serial.print("PH");
  Serial.print(",");
  Serial.print(PH);
  Serial.print(",");

  Serial.print("TSS");
  Serial.print(",");
  Serial.print(TSS);
  Serial.print(",");

  Serial.print("T");
  Serial.print(",");
  Serial.print(Temp);
  Serial.print(",");

  Serial.print("WWF");
  Serial.print(",");
  Serial.print(WWF);
  Serial.print(",");

  Serial.print("TDS");
  Serial.print(",");
  Serial.print(TDS);
  Serial.print(",");

}

void loop() {
  read_sensor();
  send_data();
  delay(250);

}
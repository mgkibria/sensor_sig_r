#include <OneWire.h>
OneWire ds(7);

//definitions for pHmeter
#define SensorPin A0            //pH meter Analog output to Arduino Analog Input 0
#define Offset 0.00            //deviation compensate
#define samplingInterval 20
#define printInterval 250
#define ArrayLenth  10    //times of collection

//definitions for temperature
#define DS18S20_ID 0x10
#define DS18B20_ID 0x28

//global variables for pH
int pHArray[ArrayLenth];   //Store the average value of the sensor feedback
int pHArrayIndex = 0;
static float pHValue;

//global variables for Temperature
float temp;

//other global variables
int TSS, TDS;
volatile double waterFlow;


void pH_read() {
  static unsigned long samplingTime = millis();
  static float voltage_ph;
  if (millis() - samplingTime > samplingInterval)
  {
    pHArray[pHArrayIndex++] = analogRead(SensorPin);
    if (pHArrayIndex == ArrayLenth)pHArrayIndex = 0;
    voltage_ph = avergearray(pHArray, ArrayLenth) * 5.0 / 1024;
    pHValue = 3.5 * voltage_ph + Offset;
    samplingTime = millis();
  }
}

void pH_print() {
  Serial.print("PH");
  Serial.print(",");
  Serial.print(pHValue, 2);
  Serial.print(",");
}

void TSS_read() {
  int sensorValue = analogRead(A1);// read the input on analog pin A1:
  float voltage_tss = sensorValue * (5.0 / 1024.0); // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  TSS = (-1120.4 * (voltage_tss * voltage_tss)) + (5742.3 * voltage_tss) - 4352.9;
}

void TSS_print() {
  Serial.print("TSS");
  Serial.print(",");
  Serial.print(TSS);
  Serial.print(",");
}

//temperature measurement
boolean getTemperature() {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  //find a device
  if (!ds.search(addr)) {
    ds.reset_search();
    //Serial.print("Problem at 1,");
    return false;
  }
  if (OneWire::crc8( addr, 7) != addr[7]) {
    //Serial.print("Problem at 2,");
    return false;
  }
  if (addr[0] != DS18S20_ID && addr[0] != DS18B20_ID) {
    //Serial.print("Problem at 3,");
    return false;
  }
  ds.reset();
  ds.select(addr);
  // Start conversion
  ds.write(0x44, 1);
  // Wait some time...
  delay(850);
  present = ds.reset();
  ds.select(addr);
  // Issue Read scratchpad command
  ds.write(0xBE);
  // Receive 9 bytes
  for ( i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
  // Calculate temperature value
  temp = ( (data[1] << 8) + data[0] ) * 0.0625;
  return true;
}

void Temp_read(){
  getTemperature();
}

void Temp_print() {
  Serial.print("T");
  Serial.print(",");
  if (getTemperature()){
  Serial.print(temp);
  Serial.print(",");
  }
  else Serial.print ("27.0,");
}

void WWF_print() {
  Serial.print("WWF");
  Serial.print(",");
  Serial.print(waterFlow);
  Serial.print(",");
}


void setup() {
  Serial.begin(9600);
  waterFlow = 0;
  attachInterrupt(0, pulse, RISING);  //DIGITAL Pin 2: Interrupt 0

}

void loop() {
  pH_read();
  TSS_read();
  Temp_read();
  //WWF_read();
  //TDS_read();

  pH_print();
  TSS_print();
  Temp_print();
  WWF_print();
  //TDS_print();

}


void pulse()   //measure the quantity of square wave
{
  waterFlow += 1.0 / 450.0;
}

double avergearray(int* arr, int number) {
  int i;
  int max, min;
  double avg;
  long amount = 0;
  if (number <= 0) {
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if (number < 5) { //less than 5, calculated directly statistics
    for (i = 0; i < number; i++) {
      amount += arr[i];
    }
    avg = amount / number;
    return avg;
  } else {
    if (arr[0] < arr[1]) {
      min = arr[0]; max = arr[1];
    }
    else {
      min = arr[1]; max = arr[0];
    }
    for (i = 2; i < number; i++) {
      if (arr[i] < min) {
        amount += min;      //arr<min
        min = arr[i];
      } else {
        if (arr[i] > max) {
          amount += max;  //arr>max
          max = arr[i];
        } else {
          amount += arr[i]; //min<=arr<=max
        }
      }//if
    }//for
    avg = (double)amount / (number - 2);
  }//if
  return avg;
}